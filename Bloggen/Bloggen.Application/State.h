#pragma once

#include <QObject>
#include <QVariant>

class State : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QVariant client MEMBER m_client NOTIFY clientChanged)
    QVariant m_client;
    Q_SIGNAL void clientChanged();
};
