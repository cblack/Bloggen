// SPDX-FileCopyrightText: 2020 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <QApplication>
#include <QCoreApplication>
#include <QIcon>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include <KLocalizedString>
#include <KLocalizedContext>

#include "State.h"

auto main(int argc, char *argv[]) -> int
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    auto app = new QApplication(argc, argv);

    KLocalizedString::setApplicationDomain("org.kde.Bloggen");

    QApplication::setOrganizationName("KDE");
    QApplication::setOrganizationDomain("org.kde");
    QApplication::setApplicationName("Bloggen");

    QApplication::setWindowIcon(QIcon::fromTheme(QString("org.kde.Bloggen")));
    QApplication::setDesktopFileName("org.kde.Bloggen.desktop");

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.rootContext()->setContextProperty(QStringLiteral("State"), new State);
    const QUrl url(QStringLiteral("qrc:/Application.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     app, [url](QObject *obj, const QUrl &objUrl) {
        if ((obj == nullptr) && url == objUrl) {
            QCoreApplication::exit(-1);
        }
    }, Qt::QueuedConnection);
    engine.load(url);

    return QApplication::exec();
}
