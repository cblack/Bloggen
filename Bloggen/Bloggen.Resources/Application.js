// SPDX-FileCopyrightText: 2020 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

/**
 * Send an HTTP request
 * @param {{method: ?string, headers: ?Object.<string,any>, url: string, body: string}} obj
 */
let sendRequest = obj => {
	return new Promise((resolve, reject) => {
		try {
			let xhr = new XMLHttpRequest()
			xhr.open(obj.method || "GET", obj.url)
			if (obj.headers) {
				for (const key in obj.headers) {
					xhr.setRequestHeader(key, obj.headers[key])
				}
			}
			xhr.onload = () => {
				if (xhr.status >= 200 && xhr.status < 300) {
					print("accepting...")
					resolve(xhr.response)
				} else {
					print("denying...")
					reject({ "status": xhr.status, "response": xhr.response, "statusText": xhr.statusText })
				}
			}
			xhr.onerror = () => reject(xhr.statusText)
			xhr.send(obj.body)
		} catch (error) {
			reject(error)
		}
	})
}

let stringme = obj => JSON.stringify(obj)

class Client {
	/**
	 * @param {string} server
	 */
	constructor(server, router, username = "", authToken = "") {
		this.server = server
		this.username = username
		this.authToken = authToken
		this.router = router
	}
	get api() {
		return `${this.server}/api`
	}
	get authorization() {
		return `Token ${this.authToken}`
	}
	/**
	 * @param {string} username The username to log in with
	 * @param {string} password The password to log in with
	 * 
	 * @returns {Promise<{username: string, authToken: string}>}
	 */
	login(username, password) {
		return sendRequest({
			"method": "POST",
			"headers": {
				"Content-Type": "application/json"
			},
			"url": `${this.api}/auth/login`,
			"body": stringme({
				"alias": username,
				"pass": password
			})
		}).then(data => {
			this.username = username
			this.authToken = JSON.parse(data)["data"]["access_token"]
			router.navigateToRoute({ "route": "compose", "data": this })

			return {
				username: this.username,
				authToken: this.authToken
			}
		})
	}
	/**
	 * @param {string} title - The post's title
	 * @param {string} body - The body of the post
	 */
	publish(title, body) {
		return sendRequest({
			"method": "POST",
			"headers": {
				"Content-Type": "application/json",
				"Authorization": this.authorization,
			},
			"url": `${this.api}/posts`,
			"body": stringme({
				"body": body,
				"title": title
			})
		})
	}
}

var ClientInstance

/**
 * Log into a WriteFreely server
 * @param {string} server - The base URL of the server
 * @param {string} username - The user's username
 * @param {string} password - The user's password
 * @returns {Promise<{client: Client, data: {username: string, authToken: string}}}
 */
function login(server, username, password, router) {
	let client = new Client(server, router)
	return client.login(username, password).then(data => {
		ClientInstance = client

		return {
			client: client,
			data: data
		}
	})
}

/**
 * Log into a WriteFreely server with a token
 * @param {string} server - The base URL of the server
 * @param {string} username - The user's username
 * @param {string} token - The user's token
 * @returns {Client}
 */
function tokened(server, username, token, router) {
	let client = new Client(server, router, username, token)
	ClientInstance = client
	return client
}

Promise.prototype.finally = Promise.prototype.finally || {
	finally(fn) {
		const onFinally = callback => Promise.resolve(fn()).then(callback);
		return this.then(
			result => onFinally(() => result),
			reason => onFinally(() => Promise.reject(reason))
		);
	}
}.finally;
