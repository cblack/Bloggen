// SPDX-FileCopyrightText: 2020 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import QtQuick 2.0
import QtQuick.Layouts 1.10
import QtQuick.Controls 2.10 as QQC2
import org.kde.kirigami 2.12 as Kirigami

import "Application.js" as App

Kirigami.Page {
    background: Item {}

    ColumnLayout {
        anchors.centerIn: parent

        Kirigami.Heading {
            text: i18n("Log Into Write Freely")

            Layout.alignment: Qt.AlignHCenter
        }

        Item { Layout.preferredHeight: Kirigami.Units.largeSpacing }

        QQC2.TextField {
            id: serverField
            placeholderText: i18n("Provider URL (e.g. https://write.as)")

            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter
        }
        QQC2.TextField {
            id: userField
            placeholderText: i18n("Username")

            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter
        }
        QQC2.TextField {
            id: passField
            placeholderText: i18n("Password")

            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter
        }

        Item { Layout.preferredHeight: Kirigami.Units.largeSpacing }

        QQC2.Button {
            id: loginButton
            text: i18n("Log In")

            Layout.alignment: Qt.AlignHCenter

            onClicked: {
                loginButton.enabled = false

                App.login(serverField.text, userField.text, passField.text, Kirigami.PageRouter).then(client => {
                    window.sets.server = serverField.text
                    window.sets.username = client.data.username
                    window.sets.token = client.data.authToken

                    State.client = client.client
                }).catch(err => {
                    applicationWindow().showPassiveNotification(JSON.stringify(err))
                }).finally(() => loginButton.enabled = true)
            }
        }
    }
}