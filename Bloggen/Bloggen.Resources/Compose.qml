// SPDX-FileCopyrightText: 2020 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import QtQuick 2.0
import QtQuick.Layouts 1.10
import QtQuick.Controls 2.10 as QQC2
import org.kde.kirigami 2.14 as Kirigami

import "Application.js" as App

Kirigami.Page {
    background: Item {}

    Kirigami.FlexColumn {
        anchors.fill: parent

        TextEdit {
            id: blogEditor

            text: ""

            wrapMode: TextEdit.Wrap
            focus: true

            font.bold: false
            font.pixelSize: Kirigami.Units.gridUnit * 1.25

            color: Kirigami.Theme.textColor
            selectedTextColor: Kirigami.Theme.highlightedTextColor
            selectionColor: Kirigami.Theme.highlightColor

            Layout.fillWidth: true
            Layout.fillHeight: true
        }
    }

    QQC2.RoundButton {
        id: submitButton
        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
            bottomMargin: Kirigami.Units.gridUnit * 2
        }
        width: Kirigami.Units.gridUnit * 3
        height: width
        radius: width / 2
        icon.name: "document-send"
        onClicked: {
            this.enabled = false

            State.client.publish("Yeet", blogEditor.text).then(_data => {
                blogEditor.text = ""
                applicationWindow().showPassiveNotification("Successfully published blog post!")
            }).catch(err => {
                applicationWindow().showPassiveNotification(`Error publishing post: ${JSON.stringify(err)}`)
            }).finally(() => submitButton.enabled = true)
        }
    }
}