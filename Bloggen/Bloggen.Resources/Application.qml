// SPDX-FileCopyrightText: 2020 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import QtQuick 2.0
import QtQuick.Window 2.15
import org.kde.kirigami 2.12 as Kirigami

import Qt.labs.settings 1.0

Kirigami.RouterWindow {
    id: window

    property var sets: Settings {
        property string server
        property string username
        property string token
    }

    initialRoute: "login"
    controlsVisible: false
    pageStack.globalToolBar.style: Kirigami.ApplicationHeaderStyle.None
    flags: Qt.FramelessWindowHint | Qt.WA_TranslucentBackground
    color: "transparent"

    pageStack.background: Rectangle {
        color: Kirigami.Theme.backgroundColor
        border {
            width: window.visibility === Window.Maximized ? 0 : 2
            color: Kirigami.ColorUtils.scaleColor(Kirigami.Theme.textColor, {"alpha": -80.0})
        }
        radius: window.visibility === Window.Maximized ? 0 : 10
    }

    Kirigami.PageRoute {
        name: "login"
        Login {}
    }

    Kirigami.PageRoute {
        name: "compose"
        Compose {}
    }
}