cmake_minimum_required(VERSION 3.5)

project(Bloggen LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

include(GNUInstallDirs)

set(REQUIRED_QT_VERSION 5.13.0)

find_package(Qt5 COMPONENTS Core Widgets DBus Quick Concurrent Multimedia REQUIRED)
find_package(Qt5QuickCompiler)
find_package(KF5Kirigami2)
find_package(KF5ConfigWidgets)
find_package(KF5I18n)

qtquick_compiler_add_resources(RESOURCES Bloggen/Bloggen.Resources/Resources.qrc)
qt5_wrap_cpp(MOC_FILES
  Bloggen/Bloggen.Application/State.h
)

add_executable(org.kde.Bloggen
  Bloggen/Bloggen.Application/Bloggen.cpp
  Bloggen/Bloggen.Application/State.h
  ${MOC_FILES}
  ${RESOURCES}
)

target_link_libraries(org.kde.Bloggen
  PRIVATE Qt5::Core Qt5::Widgets Qt5::DBus Qt5::Quick Qt5::Concurrent Qt5::Multimedia
    KF5::I18n KF5::Kirigami2 KF5::ConfigWidgets)

install(TARGETS org.kde.Bloggen DESTINATION ${CMAKE_INSTALL_BINDIR})
install(FILES Bloggen.Metadata/org.kde.Bloggen.desktop DESTINATION share/applications)
install(FILES Bloggen.Metadata/org.kde.Bloggen.svg DESTINATION share/icons/hicolor/scalable/apps)
install(FILES Bloggen.Metadata/org.kde.Bloggen.appdata.xml DESTINATION share/metainfo)
