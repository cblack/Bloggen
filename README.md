# Bloggen

Bloggen is a distraction-free WriteFreely client.

## Features

- Minimal UI: Bloggen only shows you a text field and a submit button when writing: everything else is excess.
